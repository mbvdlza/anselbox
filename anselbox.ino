
#include "lib/multiCameraIrControl.h"



Nikon NikonIR(6);
int ledOnBoard = 13;

void setup(){
  pinMode(ledOnBoard, OUTPUT);
}

void loop(){

  digitalWrite(ledOnBoard, HIGH);
  delay(50);
  digitalWrite(ledOnBoard, LOW);
  delay(150);
  digitalWrite(ledOnBoard, HIGH);
  delay(100);
  digitalWrite(ledOnBoard, LOW);
  delay(150);
  digitalWrite(ledOnBoard, HIGH);
  delay(400);
  digitalWrite(ledOnBoard, LOW);
  delay(550);
  
  digitalWrite(ledOnBoard, HIGH);
  NikonIR.trigger();
  delay(200);
  digitalWrite(ledOnBoard, LOW);
  delay(5000);
}
