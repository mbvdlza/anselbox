/**

   cameraIrControl Library originally by Sebastian Setz 2010-12-16 - http://Sebastian.Setz.name
   Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
   To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/

   Adapted and modified by Marlon v/d Linde <marlon@notnull.xyz> http://notnull.xyz for the AnselBox project
*/

#ifndef multiCameraIrControl_h
#define multiCameraIrControl_h

#include <Arduino.h>

class Nikon
{
  public:
    Nikon(int pin);
    void trigger();

  private:
    int _pin;
    int _freq;
};

class Canon
{
  public:
    Canon(int pin);
    void trigger();
    void triggerDelayed();

  private:
    int _pin;
    int _freq;
};

class Pentax
{
  public:
    Pentax(int pin);
    void trigger();

  private:
    int _pin;
    int _freq;
};

class Olympus
{
  public:
    Olympus(int pin);
    void trigger();

  private:
    int _pin;
    int _freq;
};

class Minolta
{
  public:
    Minolta(int pin);
    void trigger();
    void triggerDelayed();

  private:
    int _pin;
    int _freq;
};

class Sony
{
  public:
    Sony(int pin);
    void trigger();
    void triggerDelayed();

  private:
    int _pin;
    int _freq;
};

#endif
